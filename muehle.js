/*

Spielregeln:
1. Das Spielfeld besteht aus 24 Punkten, die nach einem bestimmten Muster verbunden sind.
2. Jeder Spieler hat zu Beginn 9 Steine auf der Hand.
3. Spieler 1 setzt einen Stein auf einen freien Punkt.
4. Spieler 2 setzt einen Stein auf einen freien Punkt.
5. Der 3. und 4. Schritt werden solange wiederholt, bis beide Spieler keine Steine mehr in der Hand haben.
6. Spieler 1 bewegt einen seiner Steine auf einen angrenzenden freien Punkt.
7. Falls der Spieler nur noch genau 3 Steine hat, darf er den Stein auf einen beliebigen freien Punkt setzen.
8. Falls er keinen Stein bewegen kann, hat der Spieler verloren.
9. Drei Steine eines Spielers, die in einer Geraden auf Punkten nebeneinader liegen, nennt man "Mühle".
10. Falls der Spieler eine Mühle schließt, darf er einen beliebigen Stein des anderen Spielers aus dem Spiel nehmen, sofern dieser Stein nicht ebenfalls Bestandteil einer Mühle ist.
11. Falls der andere Spieler nur geschlossene Mühlen hat, ist es erlaubt auch einen Stein daraus zu nehmen.
12. Falls der andere Spieler nach dem Nehmen eines Steins nur noch 2 Steine hat, hat er verloren.
13. Danach beginnt der andere Spieler ab dem 6. Schritt.

Positionen der Punkte auf dem Spielfeld (x:y)
1:1         4:1         7:1
    2:2     4:2     6:2
        3:3 4:3 5:3
1:4 2:4 3:4     5:4 6:4 7:4
        3:5 4:5 5:5
    2:6     4:6     6:6
1:7         4:7         7:7

Nummern der Punkte auf dem Spielfeld
01          02          03
    04      05      06
        07  08  09
10  11  12      13  14  15
        16  17  18
    19      20      21
22          23          24

*/

'use strict';

/** @constant {undefined} keinSpieler - Der Wert, wenn kein Spieler auf einem Punkt ist */
const keinSpieler = undefined;
/** @constant {undefined} keinStein - Der Wert, wenn kein Stein in der Hand ist */
const keinStein = undefined;

// Angaben für die Punkte in Pixel
const durchmesser = 30;
const rand = 1;
const abstand = 50;
const faktor = durchmesser + 2 * rand + abstand;

/**
 * @type {Punkt[]}
 * @description Alle Punkte auf dem Spielfeld
 * @global
 */
let punkte;
/**
 * @type {Linie[]}
 * @description linien - Alle Linien auf dem Spielfeld
 * @global
 */
let linien;
/**
 * @type {Spieler}
 * @description Der erste Spieler
 * @global
 */
let spieler1;
/**
 * @type {Spieler}
 * @description Der zweite Spieler
 * @global
 */
let spieler2;
/**
 * @type {Spieler}
 * @description Der Spieler, der gerade am Zug ist
 * @global
 */
let aktuellerSpieler;
/**
 * @type {boolean}
 * @description ```true``` falls sich das Spiel noch in der Anfangsphase befindet
 * @global
 */
let istAnfang;
/**
 * @type {Punkt}
 * @description Der Punkt, dessen Stein sich gerade in der Hand des {@link aktuellerSpieler} befindet
 * @global
 */
let steinInHand;
/**
 * @type {boolean}
 * @description ```true``` falls eine Mühle in einem Spielzug geschlossen wurde
 * @global
 */
let istNeueMühle;
/**
 * @type {boolean}
 * @description ```true``` falls gerade geklickt werden darf
 * @global
 */
let istKlickErlaubt = true;

/**
 * Ein Punkt auf dem Spielfeld
 * @constructor
 * @param {number} x - Die horizontale Position des Punkts
 * @param {number} y - Die vertikale Position des Punkts
 */
function Punkt(x, y) {
  /**
   * Die horizontale Position des Punkts
   * @type {number}
   */
  this.x = x;
  /**
   * Die vertikale Position des Punkts
   * @type {number}
   */
  this.y = y;
  /**
   * Der Spieler auf diesem Punkt
   * @type {Spieler|keinSpieler}
   */
  this.spieler = keinSpieler;
  /**
   * Die Linien, die sich mit diesem Punkt kreuzen
   * @type {Linie[]}
   */
  this.linien = [];
  /**
   * Die benachbarten Punkte
   * @type {Punkt[]}
   */
  this.nachbarn = [];
  /**
   * Das Element, das diesen Punkt im HTML-Dokument darstellt
   * @type {Node}
   */
  this.div = undefined;
}

/**
 * Eine Linie auf dem Spielfeld
 *
 * Merkt sich auch diese Linie und die benachbarten Punkte für die angegebenen Punkte
 *
 * @constructor
 * @param {number} nummer1 - Ein äußerer Punkt der Linie
 * @param {number} nummer2 - Der mittlere Punkt der Linie
 * @param {number} nummer3 - Der andere äußere Punkt der Linie
 */
function Linie(nummer1, nummer2, nummer3) {
  /**
   * Die Punkte auf dieser Linie
   * @type {Punkt[]}
   */
  this.punkte = [
    punkte[nummer1 - 1],
    punkte[nummer2 - 1],
    punkte[nummer3 - 1]
  ];

  this.punkte.forEach(
    (punkt) => {
      merkeLinie(punkt, this);
      merkeNachbarn(punkt, this.punkte);
    }
  );
}

/**
 * Ein Mitspieler
 * @constructor
 * @param {string} name - Der Name des Spielers
 * @param {number} spielerNummer - Die Spielernummer
 */
function Spieler(name, spielerNummer) {
  /**
   * Der Name des Spielers
   * @type {string}
   */
  this.name = name;
  /**
   * Die CSS-Klasse des Spielers
   * @type {string}
   */
  this.className = `spieler-${spielerNummer}`;
  /**
   * Die Anzahl der Steine, die der Spieler noch in seiner Hand hat
   * @type {number}
   */
  this.steineInHand = 9;
  /**
   * Die Anzahl der Steine, die noch nicht weggenommen wurden
   * @type {number}
   */
  this.anzahlSteine = this.steineInHand;
}

/**
 * Merkt sich eine Linie für einen Punkt
 * @param {Punkt} punkt
 * @param {Linie} linie
 */
function merkeLinie(punkt, linie) {
  punkt.linien.push(linie);
}

/**
 * Merkt sich benachbarte Punkte für einen Punkt
 * @param {Punkt} punkt - Der Punkt, für den sich die Nachbarn gemerkt wird
 * @param {Punkt[]} punkte - Die benachbarten Punkte **inklusive** dem Punkt, für den sich die Nachbarn gemerkt werden
 */
function merkeNachbarn(punkt, punkte) {
  const position = punkte.indexOf(punkt);
  if (position === 0) {
    merkeNachbar(punkt, punkte[1]);
  } else if (position === 1) {
    merkeNachbar(punkt, punkte[0]);
    merkeNachbar(punkt, punkte[2]);
  } else if (position === 2) {
    merkeNachbar(punkt, punkte[1]);
  }
}

/**
 * Merkt sich einen benachbarten Punkt
 * @param {Punkt} punkt - Der Punkt, für den sich die Nachbarn gemerkt werden
 * @param {Punkt} nachbar - Der benachbarte Punkt
 */
function merkeNachbar(punkt, nachbar) {
  if (!punkt.nachbarn.includes(nachbar)) {
    punkt.nachbarn.push(nachbar);
  }
}

/**
 * Zeichnet alle Linien
 */
function zeichneLinien() {
  linien.forEach(
    (linie) => {
      const anfang = linie.punkte[0];
      const ende = linie.punkte[2];

      const div = document.createElement('div');
      div.classList.add('linie');

      const istLinksRechts = anfang.y === ende.y;
      if (istLinksRechts) {
        div.style.height = '1px';
        div.style.width = `${rechneBreite(anfang, ende)}px`;
        div.style.left = `${rechneLinks(anfang)}px`;
        div.style.top = `${rechneOben(anfang) + durchmesser / 2}px`;
      } else {
        div.style.width = '1px';
        div.style.height = `${rechneHöhe(anfang, ende)}px`;
        div.style.left = `${rechneLinks(anfang) + durchmesser / 2}px`;
        div.style.top = `${rechneOben(anfang)}px`;
      }

      document.body.appendChild(div);
    }
  );
}

/**
 * Berechnet die Breite in Pixel zwischen zwei horizontale Positionen
 * @param {Punkt} anfang
 * @param {Punkt} ende
 * @returns {number}
 */
function rechneBreite(anfang, ende) {
  return (ende.x - anfang.x) * faktor;
}

/**
 * Berechnet die Höhe in Pixel zwischen zwei vertikalen Positionen
 * @param {Punkt} anfang
 * @param {Punkt} ende
 * @returns {number}
 */
function rechneHöhe(anfang, ende) {
  return (ende.y - anfang.y) * faktor;
}

/**
 * Berechnet die horizontale Position in Pixel
 * @param {Punkt} punkt
 * @returns {number}
 */
function rechneLinks(punkt) {
  return punkt.x * faktor;
}

/**
 * Berechnet die vertikale Position in Pixel
 * @param {Punkt} punkt
 * @returns {number}
 */
function rechneOben(punkt) {
  return punkt.y * faktor;
}

/**
 * Zeichnet alle Punkte und leitet die Mausklicks weiter
 */
function zeichnePunkte() {
  punkte.forEach(
    (punkt) => {
      const div = document.createElement('div');
      div.classList.add('punkt');
      div.style.width = `${durchmesser}px`;
      div.style.height = `${durchmesser}px`;
      div.style.left = `${rechneLinks(punkt)}px`;
      div.style.top = `${rechneOben(punkt)}px`;
      div.addEventListener('click', async () => {
        if (!istKlickErlaubt) {
          alert("Du darfst gerade nicht klicken!");
          return;
        }

        istKlickErlaubt = false;
        await klickePunkt(aktuellerSpieler, punkt);
        istKlickErlaubt = true;
      });
      document.body.appendChild(div);

      punkt.div = div;
    }
  );
}

/**
 * Zeichnet die Anzeige des aktuellen Spielers
 */
function zeichneAktuellerSpieler() {
  const div = document.createElement('div');
  div.id = 'aktueller-spieler';
  div.style.left = `${rechneLinks(punkte[0])}px`;
  div.style.top = `${durchmesser}px`;
  document.body.appendChild(div);
}

/**
 * Zeichnet alle sonstigen Knöpfe
 */
function zeichneKnöpfe() {
  zeichneKnopf("Frage nach neuen Spielernamen", frageNachNeuenSpielerNamen);
  zeichneKnopf("Fange von vorne an", () => {
    if (confirm("Möchtet ihr wirklich von vorne anfangen?")) {
      vonVorne();
    }
  });
}

/**
 * Zeichnet einen Knopf
 * @param {string} beschriftung - Das, was auf dem Knopf steht
 * @param {function} aktion - Die Aktion, die nach dem Klick passieren soll
 */
function zeichneKnopf(beschriftung, aktion) {
  const button = document.createElement('button');
  button.type = 'button';
  button.addEventListener('click', aktion);
  button.appendChild(document.createTextNode(beschriftung));
  document.body.appendChild(button);
}

/**
 * Vergisst alles und fragt nach neuen Spielernamen
 */
function frageNachNeuenSpielerNamen() {
  vergissGemerktes();
  spieler1.name = gemerkterSpielerName(1);
  spieler2.name = gemerkterSpielerName(2);
  setzeAktuellenSpieler(aktuellerSpieler);
}

/**
 * Vergisst alles, was über ein Neuladen hinaus gemerkt wurde
 */
function vergissGemerktes() {
  localStorage.clear();
}

/**
 * Meldet den Sieg eines Spielers
 *
 * Startet außerdem das Spiel nach der Meldung auf
 *
 * @param {Spieler} spieler - Der Spieler, der gewonnen hat
 */
function meldeSieg(spieler) {
  alert(`Herzlichen Glückwunsch, ${spieler.name}! Du hast gewonnen!`);
  vonVorne();
}

/**
 * Räumt das Spielfeld leer
 */
function räumeAuf() {
  while (document.body.childNodes.length) {
    document.body.removeChild(document.body.childNodes[0]);
  }
}

/**
 * Prüft, ob ein Spieler gewonnen hat, weil der Gegner zuwenige Steine übrig hat
 * @param {Spieler} spieler - Der Spieler, dessen Sieg geprüft wird
 * @return {boolean} ```true``` falls ```spieler``` gewonnen hat
 */
function prüfeSiegDurchAnzahlSteine(spieler) {
  return gibAnderenSpieler(spieler).anzahlSteine < 3;
}

/**
 * Prüft, ob ein Spieler gewonnen hat, weil der Gegner keinen Zug mehr machen kann
 * @param {Spieler} spieler - Der Spieler, dessen Sieg geprüft wird
 * @return {boolean} ```true``` falls ```spieler``` gewonnen hat
 */
function prüfeSiegDurchUnbeweglichkeit(spieler) {
  const andererSpieler = gibAnderenSpieler(spieler);
  const punkte = gibSpielerPunkte(andererSpieler);
  return !punkte.some(punkt => prüfeBeweglichkeit(andererSpieler, punkt));
}

/**
 * Findet alle Punkte eines Spielers
 * @param {Spieler} spieler - Der Spieler, dessen Punkte gefunden werden
 * @return {Punkt[]}
 */
function gibSpielerPunkte(spieler) {
  return punkte.filter(punkt => punkt.spieler === spieler);
}

/**
 * Prüft, ob ein Spieler einen Stein an einem Punkt bewegt werden darf
 * @param {Spieler} spieler - Der Spieler, dessen Stein geprüft wird
 * @param {Punkt} punkt - Der Punkt, dessen Stein geprüft wird
 * @return {boolean} ```true``` falls der Stein bewegt werden darf
 */
function prüfeBeweglichkeit(spieler, punkt) {
  return punkt.nachbarn.some(nachbar => prüfeBewegung(spieler, punkt, nachbar));
}

/**
 * Versucht, einen gegnerischen Stein an einem Punkt wegzunehmen
 *
 * Meldet auch, falls das Wegnehmen nicht geklappt hat
 *
 * @async
 * @param {Spieler} spieler - Der Spieler, der einen Stein wegnimmt
 * @param {Punkt} punkt - Der Punkt, dessen Stein genommen wird
 * @return {Promise<boolean>} ```true``` falls das Wegnehmen geklappt hat
 */
async function nimmSteinWeg(spieler, punkt) {
  return new Promise((resolve) => {
    if (punkt.spieler === spieler) {
      alert("Das ist dein eigener Stein! Nimm einen anderen!");
      resolve(false);
    } else if (punkt.spieler === keinSpieler) {
      alert("Hier liegt kein Stein!");
      resolve(false);
    } else if (prüfeWegnehmen(spieler, punkt)) {
      const andererSpieler = punkt.spieler;
      animiereSteinWegnehmen(punkt).then(
        () => {
          andererSpieler.anzahlSteine--;
          resolve(true);
        }
      );

      machePunktFrei(punkt);
    } else {
      alert("Du darfst diesen Stein nicht nehmen! Nimm einen anderen!");
      resolve(false);
    }
  });
}

/**
 * Animiere das Wegnehmen eines Steins
 * @return {Promise} Löst auf, wenn die Animation abgeschlossen ist
 * @param {Punkt} punkt - Der Punkt, dessen Stein genommen wird
 */
async function animiereSteinWegnehmen(punkt) {
  let div = punkt.div.cloneNode(false);
  document.body.appendChild(div);

  let i = 0;
  return animiere(
    () => {
      div.style.opacity = 1 - i;
      i += 0.1;
      return i <= 1;
    }, 25
  ).then(() => {
    document.body.removeChild(div);
  });
}

/**
 * Prüft, ob ein Stein weggenommen werden darf
 * @param {Spieler} spieler - Der Spieler, der einen Stein wegnimmt
 * @param {Punkt} punkt - Der Punkt, dessen Stein genommen wird
 * @return {boolean} ```true``` falls das Wegnehmen erlaubt ist
 */
function prüfeWegnehmen(spieler, punkt) {
  const andererSpieler = gibAnderenSpieler(spieler);
  const punkte = gibSpielerPunkte(andererSpieler);
  return punkte.every(
      punkt => prüfeMühleAmPunkt(punkt, andererSpieler)
    ) || !prüfeMühleAmPunkt(punkt, andererSpieler);
}

/**
 * Prüft, ob sich eine Mühle an einem Punkt für einen Spieler befindet
 * @param {Punkt} punkt - Der Punkt, der geprüft wird
 * @param {Spieler} spieler - Der Spieler, dessen Steine geprüft werden
 * @return {boolean} ```true``` falls sich an dem Punkt eine Mühle für den Spieler befindet
 */
function prüfeMühleAmPunkt(punkt, spieler) {
  return punkt.linien.some(linie => prüfeMühle(linie, spieler));
}

/**
 * Prüft, ob sich eine Mühle auf einer Linie für einen Spieler befindet
 * @param {Linie} linie - Die Linie, der geprüft wird
 * @param {Spieler} spieler - Der Spieler, dessen Steine geprüft werden
 * @return {boolean} ```true``` falls sich auf der Linie eine Mühle für den Spieler befindet
 */
function prüfeMühle(linie, spieler) {
  return linie.punkte.every(punkt => punkt.spieler === spieler);
}

/**
 * Findet den anderen Spieler
 * @param {Spieler} spieler - Der Spieler, dessen Gegenspieler gefunden wird
 * @return {Spieler} Der andere Spieler
 */
function gibAnderenSpieler(spieler) {
  if (spieler === spieler1) {
    return spieler2;
  } else {
    return spieler1;
  }
}

/**
 * Versucht, einen neuen Stein auf dem Spielfeld zu setzen
 *
 * Meldet außerdem, falls das Setzen nicht geklappt hat.
 *
 * @param {Spieler} spieler - Der Spieler, der den Stein setzt
 * @param {Punkt} punkt - Der Punkt, auf den der Stein gesetzt wird
 * @return {boolean} ```true``` falls das Setzen geklappt hat
 */
function setzeNeuenStein(spieler, punkt) {
  if (punkt.spieler === keinSpieler) {
    spieler.steineInHand--;
    punkt.spieler = spieler;
    punkt.div.classList.add(spieler.className);
    return true;
  } else {
    alert("Hier liegt schon ein Stein!");
  }
}

/**
 * Wechselt den aktuellen Spieler zum anderen Spieler
 *
 * @param {Spieler} spieler - Der Spieler, der gerade noch dran ist
 */
function wechselSpieler(spieler) {
  setzeAktuellenSpieler(gibAnderenSpieler(spieler));
}

/**
 * Setzt den aktuellen Spieler
 *
 * @param {Spieler} spieler - Der Spieler, der als nächstes dran ist
 */
function setzeAktuellenSpieler(spieler) {
  aktuellerSpieler = spieler;
  const div = document.getElementById('aktueller-spieler');
  div.classList.remove(spieler1.className, spieler2.className);
  div.classList.add(spieler.className);
  div.innerText = `${spieler.name} ist dran.`;
}

/**
 * Versucht, einen Stein zu verschieben
 *
 * Meldet außerdem, falls das Verschieben nicht geklappt hat
 *
 * @async
 * @param {Spieler} spieler - Der Spieler, der einen Stein verschiebt
 * @param {Punkt} von - Der Punkt, dessen Stein verschoben wird
 * @param {Punkt} nach - Der Punkt, auf den der Stein geschoben wird
 * @return {Promise<boolean>} ```true``` falls das Verschieben geklappt hat
 */
async function verschiebeStein(spieler, von, nach) {
  return new Promise((resolve) => {
    if (prüfeBewegung(spieler, von, nach)) {
      animiereSteinBewegung(von, nach).then(
        () => {
          nach.spieler = spieler;
          nach.div.classList.add(spieler.className);
          resolve(true);
        }
      );

      machePunktFrei(von);
    } else {
      alert("Du darfst hier nicht hin!");
      resolve(false);
    }
  });
}

/**
 * Animiere die Bewegung eines Steins
 * @async
 * @param {Punkt} von - Der Anfangspunkt
 * @param {Punkt} nach - Der Zielpunkt
 * @return {Promise} Löst auf, wenn die Animation abgeschlossen ist
 */
async function animiereSteinBewegung(von, nach) {
  let div = von.div.cloneNode(false);
  document.body.appendChild(div);

  let i = 0;
  return animiere(
    () => {
      const x = von.x * (1 - i) + nach.x * i;
      const y = von.y * (1 - i) + nach.y * i;
      const punkt = new Punkt(x, y);
      div.style.left = `${rechneLinks(punkt)}px`;
      div.style.top = `${rechneOben(punkt)}px`;
      i += 0.1;
      return i <= 1;
    }, 25
  ).then(() => {
    document.body.removeChild(div);
  });
}

/**
 * Animiert etwas
 * @async
 * @param {function} animation - Eine Funktion, die regelmäßig aufgerufen wird
 * @param {number} ms - Anzahl der Millisekunden zwischen den Funktionsaufrufen
 * @return {Promise} Löst auf, sobald ```animation``` den Wert ```false``` liefert
 */
async function animiere(animation, ms) {
  return new Promise((resolve) => {
    (function loop() {
      if (animation()) {
        window.setTimeout(loop, ms);
      } else {
        resolve();
      }
    })();
  });
}

/**
 * Macht einen Punkt frei
 *
 * @param {Punkt} punkt - Der Punkt, der frei gemacht wird
 */
function machePunktFrei(punkt) {
  punkt.div.classList.remove('in-der-hand');
  punkt.div.classList.remove(punkt.spieler.className);
  punkt.spieler = keinSpieler;
}

/**
 * Prüft, ob eine Bewegung eines Steins erlaubt ist
 * @param {Spieler} spieler - Der Spieler, der einen Stein verschiebt
 * @param {Punkt} von - Der Punkt, dessen Stein verschoben wird
 * @param {Punkt} nach - Der Punkt, auf den der Stein geschoben wird
 * @return {boolean} ```true``` falls das Verschieben erlaubt ist
 */
function prüfeBewegung(spieler, von, nach) {
  if (prüfeSpringen(spieler)) {
    return true;
  }

  if (nach.spieler === keinSpieler) {
    return prüfeNachbarn(von, nach);
  }
}

/**
 * Prüft, ob zwei Punkte nachbarn sind
 * @param {Punkt} punkt1
 * @param {Punkt} punkt2
 * @return {boolean} ```true``` falls die beiden Punkte Nachbarn sind
 */
function prüfeNachbarn(punkt1, punkt2) {
  return punkt1.nachbarn.includes(punkt2);
}

/**
 * Prüft, ob ein Spieler mit seinen Steinen springen darf
 * @param {Spieler} spieler
 * @return {boolean} ```true``` falls der Spieler springen darf
 */
function prüfeSpringen(spieler) {
  return spieler.anzahlSteine === 3;
}

/**
 * Versucht, einen Stein in die Hand zu nehmen
 *
 * Meldet außerdem, wenn das Aufnehmen nicht geklappt hat
 *
 * @param {Spieler} spieler - Der Spieler, der den Stein in die Hand nimmt
 * @param {Punkt} punkt - Der Punkt, dessen Stein in die Hand genommen wird
 */
function nimmSteinInHand(spieler, punkt) {
  if (punkt.spieler === keinSpieler) {
    alert("Hier liegt kein Stein!");
  } else if (punkt.spieler === spieler) {
    if (prüfeBeweglichkeit(spieler, punkt)) {
      punkt.div.classList.add('in-der-hand');
      steinInHand = punkt;
    } else {
      alert("Du darfst diesen Stein nicht bewegen!");
    }
  } else {
    alert("Das ist nicht dein Stein!");
  }
}

/**
 * Beginnt das Spiel von vorne
 */
function vonVorne() {
  räumeAuf();

  punkte = [
    new Punkt(1, 1), new Punkt(4, 1), new Punkt(7, 1),
    new Punkt(2, 2), new Punkt(4, 2), new Punkt(6, 2),
    new Punkt(3, 3), new Punkt(4, 3), new Punkt(5, 3),

    new Punkt(1, 4), new Punkt(2, 4), new Punkt(3, 4),
    new Punkt(5, 4), new Punkt(6, 4), new Punkt(7, 4),

    new Punkt(3, 5), new Punkt(4, 5), new Punkt(5, 5),
    new Punkt(2, 6), new Punkt(4, 6), new Punkt(6, 6),
    new Punkt(1, 7), new Punkt(4, 7), new Punkt(7, 7)
  ];

  linien = [
    new Linie(1, 2, 3),
    new Linie(4, 5, 6),
    new Linie(7, 8, 9),

    new Linie(10, 11, 12),
    new Linie(13, 14, 15),

    new Linie(16, 17, 18),
    new Linie(19, 20, 21),
    new Linie(22, 23, 24),

    new Linie(1, 10, 22),
    new Linie(4, 11, 19),
    new Linie(7, 12, 16),

    new Linie(2, 5, 8),
    new Linie(17, 20, 23),

    new Linie(9, 13, 18),
    new Linie(6, 14, 21),
    new Linie(3, 15, 24)
  ];

  spieler1 = new Spieler(gemerkterSpielerName(1), 1);
  spieler2 = new Spieler(gemerkterSpielerName(2), 2);
  istAnfang = true;
  steinInHand = keinStein;
  istNeueMühle = false;

  zeichneLinien();
  zeichnePunkte();
  zeichneAktuellerSpieler();
  zeichneKnöpfe();

  setzeAktuellenSpieler(spieler1);

  // verteileSteine();
}

/**
 * Versucht einen gemerkten Spielernamen zu nutzen oder fragt den Benutzer nach einen Namen
 * @param {number} spielerNummer - Die Nummer des Spielers, 1 oder 2
 * @return {string} Der eingegebene Spielername
 */
function gemerkterSpielerName(spielerNummer) {
  return gemerkteAbfrage(`Bitte gib deinen Namen ein, Spieler ${spielerNummer}!`);
}

/**
 * Versucht eine gemerkte Antwort für eine Abfrage zu nutzen oder fragt den Benutzer nach einer Antwort
 *
 * @param {string} frage - Die Frage, die gestellt werden soll
 * @return {string} Die Eingabe des Benutzers
 */
function gemerkteAbfrage(frage) {
  let antwort = localStorage.getItem(frage);
  if (typeof antwort === 'undefined' || antwort === null) {
    antwort = prompt(frage);
    localStorage.setItem(frage, antwort);
  }

  return antwort;
}

/**
 * Verteilt abwechselnd alle Steine auf dem Spielfeld
 */
function verteileSteine() {
  let i = 0;
  while (istAnfang) {
    klickePunkt(aktuellerSpieler, punkte[i++]);
  }
}

document.addEventListener(
  'DOMContentLoaded', () => {
    vonVorne();
  }
);

