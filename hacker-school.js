'use strict';

/**
 * Wird aufgerufen, wenn ein Punkt angeklickt wird
 *
 * @async
 * @param {Spieler} spieler - Der Spieler, der geklickt hat
 * @param {Punkt} punkt - Der Punkt, der geklickt wurde
 */
async function klickePunkt(spieler, punkt) {
  if (istNeueMühle) {
    if (!await nimmSteinWeg(spieler, punkt)) {
      return;
    }

    istNeueMühle = false;
    if (prüfeSiegDurchAnzahlSteine(spieler)) {
      meldeSieg(spieler);
      return;
    }
  } else if (istAnfang) {
    if (!setzeNeuenStein(spieler, punkt)) {
      return;
    }

    if (spieler.steineInHand === 0 && gibAnderenSpieler(spieler).steineInHand === 0) {
      istAnfang = false;
      alert("Die Anfangsphase ist beendet.");
    }
  } else if (steinInHand) {
    if (!await verschiebeStein(spieler, steinInHand, punkt)) {
      return;
    }

    steinInHand = keinStein;
    if (prüfeSiegDurchUnbeweglichkeit(spieler)) {
      meldeSieg(spieler);
      return;
    }
  } else {
    nimmSteinInHand(spieler, punkt);
    return;
  }

  if (prüfeMühleAmPunkt(punkt, spieler)) {
    istNeueMühle = true;
    alert(`Du hast eine Mühle! Nimm ${gibAnderenSpieler(spieler).name} einen Stein weg!`);
  } else {
    wechselSpieler(spieler);
  }
}
